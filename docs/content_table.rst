*************
Content table
*************

.. toctree::
    :maxdepth: 4

    installation
    user_guide
    client
    dev_guide
    extending_falcon
    api/library_root