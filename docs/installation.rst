************
Installation
************

.. toctree::
   :maxdepth: 2
   :glob:

   manual/binary_installation
   manual/docker_installation
   manual/installation