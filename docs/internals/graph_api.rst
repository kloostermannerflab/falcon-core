Graph API
=========


Context
-------

-----

.. doxygenclass:: RunContext
   :undoc-members:
   :members:

.. doxygenclass:: ProcessingContext
   :undoc-members:
   :members:

-----


Processor
---------

-----

.. doxygenclass:: IProcessor
   :undoc-members:
   :members:

-----


Datatype
--------

-----

.. doxygennamespace:: nsAnyType
   :undoc-members:
   :members:

-----

Shared states, atomic variables
-------------------------------

-----

.. doxygenclass:: IState
   :undoc-members:
   :members:

.. doxygenclass:: ReadableState
   :undoc-members:
   :members:

.. doxygenclass:: WritableState
   :undoc-members:
   :members:

-----
