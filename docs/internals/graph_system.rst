Graph system
============


.. toctree::
   :maxdepth: 1
   :glob:

   graph_life_cycle
   datastream_system
   graph_api
