Test mode
=========

To run Falcon in testing mode, set the *enabled* option to true.
