Debug
=====

To log debug messages while running Falcon, set the *enabled* option to true.

