==========
User Guide
==========

There is two configuration levels in Falcon:

- the configuration of the graph run in a Falcon session
- the configuration of the Falcon server (where to save logs, find resources ... etc.)


.. toctree::
   :maxdepth: 2

   manual/usage
   manual/graphs
   manual/configuration
   manual/troubleshoot