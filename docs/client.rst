*************
Falcon client
*************


.. toctree::
   :maxdepth: 2
   :glob:

   ui/generic_control
   ui/interaction
   ui/create_ui
   ui/lib_python